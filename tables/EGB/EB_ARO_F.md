# EB_ARO_F

Table des données de remboursements complémentaires


## Modèle de données

|Nom|Type|Description|Exemple|Propriétés|
|-|-|-|-|-|
|FLX_DIS_DTD|date|Date de mise à disposition des données|||
|REM_TYP_AFF|nombre réel|type de remboursement affiné|||
|ORG_CLE_NUM|chaîne de caractères|organisme de liquidation des prestations (avant fusion des caisses)|||
|ARO_REM_MNT|nombre réel|Montant versé-remboursé (autre que régime obligatoire)|||
|ORG_CLE_NEW|chaîne de caractères|Code de l'organisme de liquidation|||
|ARO_REM_TYP|nombre réel|Type de remboursement (autre que régime obligatoire)|||
|FLX_TRT_DTD|date|Date d'entrée des données dans le système d'information|||
|ARO_THE_TAU|nombre réel|Taux théorique de remboursement (autre que régime obligatoire)|||
|DCT_ORD_NUM|nombre réel|numéro d'ordre du décompte dans l'organisme|||
|ARO_PRS_NAT|nombre réel|Nature de la prestation (autre que régime obligatoire)|||
|ARO_ENV_TYP|nombre réel|Type d'enveloppe (autre que régime obligatoire)|||
|ARO_REM_BSE|nombre réel|Base de remboursement (autre que régime obligatoire)|||
|ARO_ASU_NAT|nombre réel|Nature d'assurance (autre que régime obligatoire)|||
|FLX_EMT_TYP|nombre réel|Type d'émetteur|||
|CLE_TEC_PRS|chaîne de caractères|Clé technique Prestation|||
|ARO_FJH_TYP|nombre réel|Type de prise en charge du forfait Journalier (autre que régime obligatoire)|||
|ARO_ORD_NUM|nombre réel|Numéro d'ordre du remboursement autre que régime obligatoire|||
|ARO_CPL_COD|nombre réel|Complément d'acte (autre que régime obligatoire)|||
|ARO_MIN_TAU|nombre réel|Taux modulé (hors parcours de Soins) du remboursement (autre que régime obligatoire)|||
|ARO_MOD_MNT|nombre réel|Montant de la majoration de la participation de l'assuré (autre que régime obligatoire)|||
|FLX_EMT_NUM|nombre réel|numéro d'émetteur du flux|||
|PRS_ORD_NUM|nombre réel|Numéro d'ordre de la prestation dans le décompte|||
|FLX_EMT_ORD|nombre réel|numéro de séquence du flux|||
|ARO_REM_SGN|nombre réel|Signe du remboursement (autre que régime obligatoire)|||
|ARO_REM_PRU|nombre réel|Prix unitaire de l'acte (autre que régime obligatoire)|||
|ARO_REM_TAU|nombre réel|Taux réel de remboursement (autre que régime obligatoire)|||
|ARO_FTA_COD|nombre réel|Code forçage du taux (hors parcours de soins) autre que régime obligatoire|||
